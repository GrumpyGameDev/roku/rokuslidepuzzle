Library "v30/bslDefender.brs"

Sub Main()
	port = CreateObject("roMessagePort")
	screen = CreateObject("roScreen",true)
	bitmap = CreateObject("roBitmap","pkg:/locale/default/images/gewnbukkit.jpg")
	subimages = CreateObject("roArray",16,false)
	positions = CreateObject("roArray",16,false)
	for column=0 to 3
		for row=0 to 3
			subimages[row*4+column]=CreateObject("roRegion",bitmap,column*320,row*180,320,180)
			positions[row*4+column]=row*4+column
		end for
	end for
	done = false
	while not done
		done = true
		for column=0 to 3
			for row=0 to 3
				if positions[row*4+column]=row*4+column then
					done = false
				end if
				if positions[row*4+column]=15 then
					open_column=column
					open_row=row
				end if
			end for
		end for
		if not done then
			next_row=open_row
			next_column=open_column
			direction = Rnd(4)
			if direction=1 then
				next_row=next_row-1
			else if direction=2 then
				next_row=next_row+1
			else if direction=3 then
				next_column=next_column-1
			else
				next_column=next_column+1
			end if
			if next_column>=0 and next_column<=3 and next_row>=0 and next_row<=3
				positions[open_row*4+open_column]=positions[next_row*4+next_column]
				positions[next_row*4+next_column]=15
			end if		
		end if
	end while
	screen.SetMessagePort(port)
	screen.SetAlphaEnable(false) 
	codes = bslUniversalControlEventCodes()
	while true
		screen.Clear(&h101010FF)
		for column=0 to 3
			for row=0 to 3
				if positions[row*4+column]<>15 then
					screen.DrawObject(column*320,row*180,subimages[positions[row*4+column]])
				else
					open_column=column
					open_row=row
				end if
			end for
		end for
		screen.Finish()
		screen.SwapBuffers()
		msg=wait(0, screen.GetMessagePort())
		if type(msg)="roUniversalControlEvent" then
			next_row=open_row
			next_column=open_column
			if msg.GetInt()=codes.BUTTON_UP_PRESSED then
				next_column=open_column
				next_row=open_row+1
			end if
			if msg.GetInt()=codes.BUTTON_DOWN_PRESSED then
				next_column=open_column
				next_row=open_row-1
			end if
			if msg.GetInt()=codes.BUTTON_LEFT_PRESSED then
				next_column=open_column+1
				next_row=open_row
			end if
			if msg.GetInt()=codes.BUTTON_RIGHT_PRESSED then
				next_column=open_column-1
				next_row=open_row
			end if
			if next_column>=0 and next_column<=3 and next_row>=0 and next_row<=3
				positions[open_row*4+open_column]=positions[next_row*4+next_column]
				positions[next_row*4+next_column]=15
			end if
            if msg.GetInt()=codes.button_instant_replay_pressed then
                Scramble(positions)
            end if
		end if
	end while
end Sub

Sub Scramble(positions)
    done = false
    while not done
        done = true
        for column=0 to 3
            for row=0 to 3
                if positions[row*4+column]=row*4+column then
                    done = false
                end if
                if positions[row*4+column]=15 then
                    open_column=column
                    open_row=row
                end if
            end for
        end for
        if not done then
            next_row=open_row
            next_column=open_column
            direction = Rnd(4)
            if direction=1 then
                next_row=next_row-1
            else if direction=2 then
                next_row=next_row+1
            else if direction=3 then
                next_column=next_column-1
            else
                next_column=next_column+1
            end if
            if next_column>=0 and next_column<=3 and next_row>=0 and next_row<=3
                positions[open_row*4+open_column]=positions[next_row*4+next_column]
                positions[next_row*4+next_column]=15
            end if      
        end if
    end while
End Sub